package com.example.engtestapp.activities;

public final class IntentKeys {

    public static final String ACTION_QUERY_SEARCH = "ACTION_QUERY_SEARCH";
    public static final String ON_CLEAR_SEARCH_VIEW = "ON_CLEAR_SEARCH_VIEW";

}
