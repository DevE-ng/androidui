package com.example.engtestapp.models.modelFixture;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.engtestapp.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.list_fixture_item)
public class FixtureItemView extends LinearLayout {

    @ViewById
    TextView tvFixtureName;

    @ViewById
    TextView tvHomeTeamName;

    @ViewById
    TextView tvAwayTeamName;

    @ViewById
    TextView tvFixtureVenue;

    @ViewById
    TextView tvFixtureDateLocal;

    @ViewById
    TextView tvDayNum;

    @ViewById
    TextView tvDayName;

    @ViewById
    TextView tvPosponded;

    public FixtureItemView(Context context)
    {
        super(context);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(layoutParams);
    }

    public void bind(Fixture fixture) {
        tvFixtureName.setText(fixture.getCompetitionName());
        tvHomeTeamName.setText(fixture.getHomeTeamName());
        tvAwayTeamName.setText(fixture.getAwayTeamName());
        tvFixtureVenue.setText(fixture.getVenueName());
        tvFixtureDateLocal.setText(fixture.getDateLocal());
        tvDayNum.setText(fixture.getDateLocalDay());
        tvDayName.setText(fixture.getDateLocalDayName());

        tvPosponded.setVisibility(View.GONE);
        tvFixtureDateLocal.setTextColor(getContext().getResources().getColor(R.color.colorText));

        if (fixture.isPostponed()) {
            tvPosponded.setVisibility(View.VISIBLE);
            tvFixtureDateLocal.setTextColor(getContext().getResources().getColor(R.color.colorWarn));
        }
    }

}
