package com.example.engtestapp.finders;

import com.example.engtestapp.models.modelResult.RVResultRow;
import com.example.engtestapp.models.modelResult.Result;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;

@EBean
public class ResultFinder {

    private ArrayList<Result> results = new ArrayList<>();
    private ArrayList<RVResultRow> rvResultsRows = new ArrayList<>();

    public void add(ArrayList<Result> results) {
        this.results = results;
    }

    public ArrayList<Result> findAll() {
        return results;
    }

    public ArrayList<RVResultRow> findAllRows() {
        rvResultsRows.clear();
        if (results.size() == 0) return new ArrayList<>();
        String latestHeader = "";
        for (Result result: results) {
            String currentHeader = result.getDateLocalHeader();

            if (latestHeader.isEmpty() || !latestHeader.equals(currentHeader)) {
                rvResultsRows.add(new RVResultRow(currentHeader, null));
                latestHeader = currentHeader;
            }

            rvResultsRows.add(new RVResultRow(null, result));
        }
        return rvResultsRows;
    }

}
