package com.example.engtestapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.example.engtestapp.finders.FixtureFinder;
import com.example.engtestapp.models.modelFixture.FixtureHeaderView;
import com.example.engtestapp.models.modelFixture.FixtureHeaderView_;
import com.example.engtestapp.models.modelFixture.FixtureItemView;
import com.example.engtestapp.models.modelFixture.FixtureItemView_;
import com.example.engtestapp.models.modelFixture.RVFixtureRow;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.List;

@EBean
public class FixtureAdapter extends RecyclerView.Adapter<ViewWrapper> implements Filterable {

    private List<RVFixtureRow> items = new ArrayList<>();
    private List<RVFixtureRow> itemsOrigin = new ArrayList<>();

    @RootContext
    Context context;

    @Bean(FixtureFinder.class)
    FixtureFinder finder;

    private static final int VIEW_HEADER = 0;
    private static final int VIEW_FIXTURE = 1;

    @AfterInject
    void initAdapter() {
        items = finder.findAllRows();
        itemsOrigin = new ArrayList<>(items);
    }

    @NonNull
    @Override
    public ViewWrapper onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_FIXTURE) return new ViewWrapper(FixtureItemView_.build(context));
        return new ViewWrapper(FixtureHeaderView_.build(context));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewWrapper viewHolder, int position) {
        RVFixtureRow fixtureRow = items.get(position);
        View view = viewHolder.itemView;
        if (view instanceof FixtureItemView) {
            FixtureItemView fiv = (FixtureItemView)view;
            fiv.bind(fixtureRow.getFixture());
        }
        if (view instanceof FixtureHeaderView) {
            FixtureHeaderView fhv = (FixtureHeaderView)view;
            fhv.bind(fixtureRow.getHeader());
        }
    }

    @Override
    public int getItemViewType(int position) {
        RVFixtureRow fixtureRow = items.get(position);
        if (fixtureRow.getFixture() != null) return VIEW_FIXTURE;
        else return VIEW_HEADER;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @UiThread
    public void updateItems(ArrayList<RVFixtureRow> items) {
        this.items = items;
        itemsOrigin = new ArrayList<>(items);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return itemsFilter;
    }

    private Filter itemsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<RVFixtureRow> itemsFiltered = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                itemsFiltered.addAll(itemsOrigin);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (RVFixtureRow item: items) {
                    if (item.getFixture() != null && item.getFixture().getCompetitionName().toLowerCase().contains(filterPattern)) {
                        itemsFiltered.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = itemsFiltered;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items.clear();
            items.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };
}