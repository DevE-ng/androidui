package com.example.engtestapp.models.modelResult;

public class RVResultRow {

    public RVResultRow(String header, Result result) {
        this.header = header;
        this.result = result;
    }

    private String header;

    private Result result;

    public Result getResult() {
        return result;
    }

    public String getHeader() {
        return header;
    }

}
