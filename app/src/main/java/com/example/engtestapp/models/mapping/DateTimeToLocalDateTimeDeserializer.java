package com.example.engtestapp.models.mapping;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.IOException;
import java.util.TimeZone;

public class DateTimeToLocalDateTimeDeserializer extends StdDeserializer<DateTime> {

    public DateTimeToLocalDateTimeDeserializer() {
        this(null);
    }

    public DateTimeToLocalDateTimeDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public DateTime deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return new DateTime(jp.getText(), DateTimeZone.forTimeZone(TimeZone.getDefault()));
    }
}