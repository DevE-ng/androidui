package com.example.engtestapp.models.modelResult;

import com.example.engtestapp.models.modelCommon.Score;
import com.example.engtestapp.models.modelFixture.Fixture;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Result extends Fixture {

    @JsonProperty("score")
    private Score score;

    private Score getScore() {
        return score;
    }

    int getAwayScore() {
        return getScore().getAwayScore();
    }

    int getHomeScore() {
        return getScore().getHomeScore();
    }

    String getWinner() {
        return getScore().getWinner();
    }

}
