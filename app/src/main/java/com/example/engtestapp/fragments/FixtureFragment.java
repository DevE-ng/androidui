package com.example.engtestapp.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.engtestapp.R;
import com.example.engtestapp.activities.IntentKeys;
import com.example.engtestapp.adapters.FixtureAdapter;
import com.example.engtestapp.data.RestClient;
import com.example.engtestapp.finders.FixtureFinder;
import com.example.engtestapp.models.modelFixture.Fixture;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;

import java.util.ArrayList;

import static com.example.engtestapp.activities.MainActivity.search_criteria;

@EFragment(R.layout.fragment_fixtures)
public class FixtureFragment extends Fragment {

    private static final String TAG = "FixtureFragment";

    @RestService
    RestClient restClient;

    @Bean(FixtureFinder.class)
    FixtureFinder finder;

    @ViewById
    RecyclerView rvFixtures;

    @ViewById
    SwipeRefreshLayout swipeToRefreshFixture;

    @Bean
    FixtureAdapter adapter;

    @AfterViews
    public void afterViews() {
       bindAdapter();
       bindSwiper();
       fetchFixtures();
    }

    void bindAdapter() {
        rvFixtures.setLayoutManager(new LinearLayoutManager(getContext()));
        rvFixtures.setAdapter(adapter);
    }

    void bindSwiper() {
        swipeToRefreshFixture.setColorSchemeColors(getContext().getResources().getColor(R.color.colorAccent));
        swipeToRefreshFixture.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchFixtures();
                sendMessageClearSearchView();
                swipeToRefreshFixture.setRefreshing(false);
            }
        });
    }

    @Background
    void fetchFixtures(){
        ArrayList<Fixture> fixtures = restClient.getFixtures();
        finder.add(fixtures);

        adapter.updateItems(finder.findAllRows());
    }

    private void sendMessageClearSearchView() {
        if (getContext() == null) return;
        Intent intent = new Intent(IntentKeys.ON_CLEAR_SEARCH_VIEW);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    @Receiver(local = true, actions = IntentKeys.ACTION_QUERY_SEARCH, registerAt = Receiver.RegisterAt.OnResumeOnPause)
    protected void onQuerySearchReceived(@Receiver.Extra(search_criteria) String criteria) {
        if(adapter != null) adapter.getFilter().filter(criteria);
    }

}
