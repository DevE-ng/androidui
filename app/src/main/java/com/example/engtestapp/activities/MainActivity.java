package com.example.engtestapp.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.engtestapp.R;
import com.example.engtestapp.adapters.TabPageAdapter;
import com.example.engtestapp.fragments.FixtureFragment_;
import com.example.engtestapp.fragments.ResultFragment_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.ViewById;

@SuppressLint("Registered")
@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public static final String search_criteria = "Competition";

    private TabPageAdapter mTabPageAdapter;

    private SearchView searchView;

    @ViewById(R.id.container)
    ViewPager mViewPager;

    @ViewById(R.id.tabs)
    TabLayout tabLayout;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @AfterViews
    void initMainActivity() {
        mTabPageAdapter = new TabPageAdapter(getSupportFragmentManager());

        setupViewPager(mViewPager);
        tabLayout.setupWithViewPager(mViewPager);

        setSupportActionBar(toolbar);
    }

    @Receiver(local = true, actions = IntentKeys.ON_CLEAR_SEARCH_VIEW)
    protected void onClearSearchView() {
        clearSearchView();
    }

    private void setupViewPager(ViewPager viewPager) {
        TabPageAdapter adapter = new TabPageAdapter(getSupportFragmentManager());
        adapter.add(new FixtureFragment_(), "Fixtures");
        adapter.add(new ResultFragment_(), "Results");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.filter_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getResources().getString(R.string.search_view_hint));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                sendSearchFilterBroadcast(newText);
                return false;
            }
        });
        return true;
    }

    public void clearSearchView() {
        if (searchView == null) return;
        toolbar.collapseActionView();
    }

    private void sendSearchFilterBroadcast(String criteria) {
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent()
                .setAction(IntentKeys.ACTION_QUERY_SEARCH)
                .putExtra(search_criteria, criteria));
    }

}