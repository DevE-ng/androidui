package com.example.engtestapp.models.modelCommon;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Venue {

    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    public String getName() {
        return name;
    }

}
