package com.example.engtestapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.example.engtestapp.finders.ResultFinder;
import com.example.engtestapp.models.modelResult.RVResultRow;
import com.example.engtestapp.models.modelResult.ResultHeaderView;
import com.example.engtestapp.models.modelResult.ResultHeaderView_;
import com.example.engtestapp.models.modelResult.ResultItemView;
import com.example.engtestapp.models.modelResult.ResultItemView_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

import java.util.ArrayList;
import java.util.List;

@EBean
public class ResultAdapter extends RecyclerView.Adapter<ViewWrapper> implements Filterable {

    private List<RVResultRow> items = new ArrayList<>();
    private List<RVResultRow> itemsOrigin = new ArrayList<>();

    @RootContext
    Context context;

    @Bean(ResultFinder.class)
    ResultFinder finder;

    private static final int VIEW_HEADER = 0;
    private static final int VIEW_RESULT = 1;

    @AfterInject
    void initAdapter() {
        items = finder.findAllRows();
        itemsOrigin = new ArrayList<>(items);
    }

    @NonNull
    @Override
    public ViewWrapper onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_RESULT) return new ViewWrapper(ResultItemView_.build(context));
        return new ViewWrapper(ResultHeaderView_.build(context));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewWrapper viewHolder, int position) {
        RVResultRow resultRow = items.get(position);
        View view = viewHolder.itemView;
        if (view instanceof ResultItemView) {
            ResultItemView riv = (ResultItemView)view;
            riv.bind(resultRow.getResult());
        }
        if (view instanceof ResultHeaderView) {
            ResultHeaderView rhv = (ResultHeaderView)view;
            rhv.bind(resultRow.getHeader());
        }
    }

    @Override
    public int getItemViewType(int position) {
        RVResultRow resultRow = items.get(position);
        if (resultRow.getResult() != null) return VIEW_RESULT;
        else return VIEW_HEADER;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @UiThread
    public void updateItems(ArrayList<RVResultRow> items) {
        this.items = items;
        itemsOrigin = new ArrayList<>(items);
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return itemsFilter;
    }

    private Filter itemsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<RVResultRow> itemsFiltered = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                itemsFiltered.addAll(itemsOrigin);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (RVResultRow item: items) {
                    if (item.getResult() != null && item.getResult().getCompetitionName().toLowerCase().contains(filterPattern)) {
                        itemsFiltered.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = itemsFiltered;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            items.clear();
            items.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };
}