package com.example.engtestapp.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.engtestapp.R;
import com.example.engtestapp.activities.IntentKeys;
import com.example.engtestapp.adapters.ResultAdapter;
import com.example.engtestapp.data.RestClient;
import com.example.engtestapp.finders.ResultFinder;
import com.example.engtestapp.models.modelResult.Result;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.rest.spring.annotations.RestService;

import java.util.ArrayList;

import static com.example.engtestapp.activities.MainActivity.search_criteria;

@EFragment(R.layout.fragment_result)
public class ResultFragment extends Fragment {

    private static final String TAG = "ResultFragment";

    @RestService
    RestClient restClient;

    @Bean(ResultFinder.class)
    ResultFinder finder;

    @ViewById
    RecyclerView rvResults;

    @ViewById
    SwipeRefreshLayout swipeToRefreshResult;

    @Bean
    ResultAdapter adapter;

    @AfterViews
    public void afterViews() {
        bindAdapter();
        bindSwiper();
        fetchResults();
    }

    void bindAdapter() {
        rvResults.setLayoutManager(new LinearLayoutManager(getContext()));
        rvResults.setAdapter(adapter);
    }

    void bindSwiper() {
        swipeToRefreshResult.setColorSchemeColors(getContext().getResources().getColor(R.color.colorAccent));
        swipeToRefreshResult.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchResults();
                sendMessageClearSearchView();
                swipeToRefreshResult.setRefreshing(false);
            }
        });
    }

    @Background
    void fetchResults(){
        ArrayList<Result> results = restClient.getResults();
        finder.add(results);

        adapter.updateItems(finder.findAllRows());
    }

    private void sendMessageClearSearchView() {
        if (getContext() == null) return;
        Intent intent = new Intent(IntentKeys.ON_CLEAR_SEARCH_VIEW);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    @Receiver(local = true, actions = IntentKeys.ACTION_QUERY_SEARCH, registerAt = Receiver.RegisterAt.OnResumeOnPause)
    protected void onQuerySearchReceived(@Receiver.Extra(search_criteria) String criteria) {
        if(adapter != null) adapter.getFilter().filter(criteria);
    }
}
