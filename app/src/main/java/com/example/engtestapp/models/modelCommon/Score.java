package com.example.engtestapp.models.modelCommon;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Score {

    @JsonProperty("home")
    private int home;

    @JsonProperty("away")
    private int away;

    @JsonProperty("winner")
    private String winner;

    public int getHomeScore() {
        return home;
    }

    public int getAwayScore() {
        return away;
    }

    public String getWinner() {
        return winner;
    }

}
