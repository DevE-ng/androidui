package com.example.engtestapp.data;

import com.example.engtestapp.models.mapping.MappingJackson2HttpMessageConverterExtended;
import com.example.engtestapp.models.modelFixture.Fixture;
import com.example.engtestapp.models.modelResult.Result;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Rest;

import org.springframework.http.converter.FormHttpMessageConverter;

import java.util.ArrayList;

@Rest(rootUrl = "https://storage.googleapis.com/cdn-og-test-api/test-task",
        converters = {
            MappingJackson2HttpMessageConverterExtended.class,
            FormHttpMessageConverter.class
        },
        responseErrorHandler = MyResponseErrorHandlerBean.class)

public interface RestClient {

    @Get("/fixtures.json")
    ArrayList<Fixture> getFixtures();

    @Get("/results.json")
    ArrayList<Result> getResults();

}
