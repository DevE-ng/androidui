package com.example.engtestapp.models.modelCommon;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.parceler.Parcel;

@Parcel(Parcel.Serialization.BEAN)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CompetitionStage {

    @JsonProperty("competition")
    private Competition competition;

    @JsonProperty("stage")
    private String stage;

    @JsonProperty("leg")
    private String leg;

    public Competition getCompetition() {
        return competition;
    }

}
