package com.example.engtestapp.finders;

import com.example.engtestapp.models.modelFixture.Fixture;
import com.example.engtestapp.models.modelFixture.RVFixtureRow;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;

@EBean
public class FixtureFinder {

    private ArrayList<Fixture> fixtures = new ArrayList<>();
    private ArrayList<RVFixtureRow> rvFixturesRows = new ArrayList<>();

    public void add(ArrayList<Fixture> fixtures) {
        this.fixtures = fixtures;
    }

    public ArrayList<Fixture> findAll() {
        return fixtures;
    }

    public ArrayList<RVFixtureRow> findAllRows() {
        rvFixturesRows.clear();
        if (fixtures.size() == 0) return new ArrayList<>();
        String latestHeader = "";
        for (Fixture fixture: fixtures) {
            String currentHeader = fixture.getDateLocalHeader();

            if (latestHeader.isEmpty() || !latestHeader.equals(currentHeader)) {
                rvFixturesRows.add(new RVFixtureRow(currentHeader, null));
                latestHeader = currentHeader;
            }

            rvFixturesRows.add(new RVFixtureRow(null, fixture));
        }
        return rvFixturesRows;
    }

}
