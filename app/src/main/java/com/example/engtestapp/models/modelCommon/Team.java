package com.example.engtestapp.models.modelCommon;

import org.parceler.Parcel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.joda.time.DateTime;

@Parcel(Parcel.Serialization.BEAN)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Team {

    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("shortName")
    private String shortName;

    @JsonProperty("abbr")
    private String abbr;

    @JsonProperty("alias")
    private String alias;

    public String getName() {
        return name;
    }

}
