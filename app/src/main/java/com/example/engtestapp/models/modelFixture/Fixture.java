package com.example.engtestapp.models.modelFixture;

import org.parceler.Parcel;

import com.example.engtestapp.models.mapping.DateTimeToLocalDateTimeDeserializer;
import com.example.engtestapp.models.modelCommon.CompetitionStage;
import com.example.engtestapp.models.modelCommon.Team;
import com.example.engtestapp.models.modelCommon.Venue;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Parcel(Parcel.Serialization.BEAN)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Fixture {

    @JsonProperty("id")
    public int id;

    @JsonProperty("type")
    private String type;

    @JsonProperty("homeTeam")
    private Team homeTeam;

    @JsonProperty("awayTeam")
    private Team awayTeam;

    @JsonProperty("date")
    @JsonDeserialize(using = DateTimeToLocalDateTimeDeserializer.class)
    private DateTime date;

    @JsonProperty("competitionStage")
    private CompetitionStage competitionStage;

    @JsonProperty("venue")
    private Venue venue;

    @JsonProperty("state")
    private String state;

    public Fixture() { }

    public String getCompetitionName() {
        return competitionStage.getCompetition().getName();
    }

    public String getHomeTeamName() {
        return homeTeam.getName();
    }

    public String getAwayTeamName() {
        return awayTeam.getName();
    }

    public String getVenueName() {
        return venue.getName();
    }

    public Date getDate() {
        return date.toDate();
    }

    public String getDateLocal() {
        return new SimpleDateFormat("MMM d, EEEE 'at' HH:mm", Locale.ENGLISH).format(getDate());
    }

    String getDateLocalDay() {
        return new SimpleDateFormat("d", Locale.ENGLISH).format(getDate());
    }

    String getDateLocalDayName() {
        return new SimpleDateFormat("MMM", Locale.ENGLISH).format(getDate());
    }

    Boolean isPostponed() {
        if (state == null) return false;
        return (state.equals("postponed"));
    }

    public String getDateLocalHeader() {
        return new SimpleDateFormat("MMMM yyyy", Locale.ENGLISH).format(getDate());
    }

}
