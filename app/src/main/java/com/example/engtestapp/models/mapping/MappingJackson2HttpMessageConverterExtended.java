package com.example.engtestapp.models.mapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

public class MappingJackson2HttpMessageConverterExtended extends MappingJackson2HttpMessageConverter {

    public MappingJackson2HttpMessageConverterExtended(){
        super();
        ObjectMapper mapper = getObjectMapper();
        mapper.registerModule(new JodaModule());
    }

    public static String convertToString(List<String> list){
        StringBuilder strBld = new StringBuilder();
        if(list!=null)
            for (String str : list) {
                strBld = strBld.append(str).append(',');
            }
        if(strBld.length()>0)
            strBld = strBld.delete(strBld.length()-1, strBld.length());
        return strBld.toString();
    }

}
