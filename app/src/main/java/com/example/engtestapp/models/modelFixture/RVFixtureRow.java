package com.example.engtestapp.models.modelFixture;

public class RVFixtureRow {

    public RVFixtureRow(String header, Fixture fixture) {
        this.header = header;
        this.fixture = fixture;
    }

    private String header;

    private Fixture fixture;

    public Fixture getFixture() {
        return fixture;
    }

    public String getHeader() {
        return header;
    }

}
