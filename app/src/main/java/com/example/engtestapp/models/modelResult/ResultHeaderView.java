package com.example.engtestapp.models.modelResult;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.engtestapp.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.list_result_header)
public class ResultHeaderView extends LinearLayout {

    @ViewById
    TextView tvGroupResultName;

    public ResultHeaderView(Context context) {
        super(context);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        setLayoutParams(layoutParams);
    }

    public void bind(String header) {
        tvGroupResultName.setText(header);
    }

}
